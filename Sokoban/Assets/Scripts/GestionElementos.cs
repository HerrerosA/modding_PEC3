﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GestionElementos : MonoBehaviour
{
    public ElementosNivel elementosNivel;
  //  public Button boton;
    public GameObject jugador;
    public GameObject caja;
    public GameObject punto;
    public GameObject pared;
    private Transform[] colocados;
    private int numeroElemento = 0;
    private Vector3 posicion;
    private string tipo;
    private GameObject tipoElementoNuevo;
    private string elegirNivel = "";
    
    
    void Awake()
    {
        elegirNivel = FindObjectOfType<ElegirNivel>().DevolverOpcion();
        
    }
    void Start()
    {
        if (System.IO.File.Exists(Application.dataPath + "/Resources/nivel_" + elegirNivel + ".json") == true)
        {
            Cargar();
        }
    }

    public void Guardar()
    {
        // Se resetean los elementos
        elementosNivel.Reset();
        // Se cogen todos los que hay dentro de Colocados
        colocados = GameObject.FindGameObjectWithTag("Colocados").GetComponentsInChildren<Transform>();
        // Se agrega cada uno
        foreach(var elemento in colocados)
        {
            elementosNivel.AgregarElemento(elemento.gameObject);
        }
        // Se guarda en un fichero json
        string json = JsonUtility.ToJson(elementosNivel, true);
        System.IO.File.WriteAllText(Application.dataPath + "/Resources/nivel_" + elegirNivel + ".json", json);
    }
    
    public void Cargar()
    {
        elementosNivel.Reset();
        numeroElemento = 0;
        var hayJugador = false;
        JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(Application.dataPath + "/Resources/nivel_" + elegirNivel + ".json"), elementosNivel);
        Scene scene = SceneManager.GetActiveScene();
        for(int i = 0; i < elementosNivel.CantidadElementos(); i++)
        {
            elementosNivel.ObtenerElemento(numeroElemento, out posicion, out tipo);
            if (tipo == "Jugador"){
                tipoElementoNuevo = jugador;
                hayJugador = true;
            }
            if (tipo == "Punto"){
                tipoElementoNuevo = punto;
            }
            if (tipo == "Caja"){
                tipoElementoNuevo = caja;
            }
            if (tipo == "Pared"){
                tipoElementoNuevo = pared;
            }
            
            if (scene.name == "Editor"){
                var altura = GameObject.FindGameObjectWithTag("Colocados").GetComponent<RectTransform>().rect.height;
                var anchura = GameObject.FindGameObjectWithTag("Colocados").GetComponent<RectTransform>().rect.width;
                posicion.x = posicion.x - (anchura/2);
                posicion.y = posicion.y - (altura/2);
            }
            GameObject objetoNuevo = Instantiate(tipoElementoNuevo, posicion, Quaternion.identity) as GameObject;
            if (scene.name == "Editor"){
                objetoNuevo.transform.SetParent(GameObject.FindGameObjectWithTag("Colocados").transform, false);
            }
            numeroElemento++;
        }
        if (hayJugador && scene.name == "Editor"){
            Destroy(jugador);
        }
      

    }
}
